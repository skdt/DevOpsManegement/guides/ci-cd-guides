
# GitLab-Runner Cache S3 setting 가이드(EKS)

본 가이드는 이미 EKS에 설치되어있는 GitLab-Runner에서 Cache에 관련된 설정사항을 추가하는 가이드 입니다.

## S3 생성 및 권한 설정

**1. 캐시를 저장할 S3 버킷을 생성합니다**
- 이미 사용할 S3 버킷이 생성되어 있는 경우 skip 합니다.
- AWS 콘솔에 로그인합니다.
- S3 서비스로 이동합니다.
- "버킷 만들기" 버튼을 클릭합니다.
- 버킷의 고유한 이름을 입력합니다.
- 버킷에 대해 원하는 리전을 선택합니다.
- 나머지 항목은 기본으로 설정한 후  "만들기" 버튼을 클릭합니다.


**2. 기존 EKS Gitlab-Runner에 연결했던 IAM 사용자에 권한을 추가합니다**

- AWS 콘솔에 접근하여 IAM을 엽니다.
- 탐색 창에서 `사용자`를 선택합니다.
- 권한을 수정하려는 사용자의 이름을 선택합니다.
- `권한` 탭을 선택한 다음 권한추가 버튼을 누릅니다.
- 기존 정책을 사용자에게 직접 연결을 선택하여 "AmazonS3FullAccess" 정책을 추가합니다.
- 다음:검토를 선택하여 정책목록을 확인하고 권한 추가를 진행합니다.

**3. Access Key , Secret key 발급**

정책이 적용된 IAM 사용자의 Access Key, Secret Key를 발급 받습니다.
추후 인증정보로 사용됩니다.

## AWS Cli 를 사용한 작업

AWS cli 사용하여 AWS에서 생성한 iam 사용자 인증을 진행합니다.

사전에 [aws cli](https://docs.aws.amazon.com/ko_kr/cli/latest/userguide/getting-started-install.html) , [kubectl](https://kubernetes.io/ko/docs/tasks/tools/)이 설치되어 있어야 합니다.


**1.aws configure 사용하여 인증**

aws configure 명령어를 사용하여 iam 사용자를 인증합니다.

```bash
$ aws configure 
AWS Access Key ID [None]:  <ACCESS KEY>
AWS Secret Access Key [None]:  <SECRET_KEY>
Default region name [None]:  
Default output format [None]:
```

**2.aws eks context인증**

aws eks 명령어를 사용하여 gitlab-runner를 설치할 eks 클러스터에 대한 context 정보를 가져옵니다.

```bash
aws eks update-kubeconfig --region ap-northeast-2 --name <Cluster-NAME>
```

정상적으로 실행되었다면 eks 관련정보를 확인합니다.

```bash
kubectl get svc
```

## GitLab-Runner Cache 설정 추가(EKS)

본 설정 항목은 GitLab-Runner를 Helm을 통해 구성햇다는 가정으로 작성되었습니다.

현재 설치된 helm version에 맞는 GitLab-runner helm chart를 설치 or 업그레이드 합니다.

helm 설치경로에서 구성에 사용된 values.yaml에서 다음과 같은 설정값을 수정합니다.

```bash
runners:
  config: |
    [[runners]]
      ...
      [runners.kubernetes]
        ...
      [runners.cache]
        Type = "s3"
        Path = "cache"
        Shared = true
        [runners.cache.s3]
          ServerAddress = "s3.amazonaws.com"
          BucketName = "my_bucket_name"
          BucketLocation = "eu-west-1"
          Insecure = false
          AuthenticationType = "access-key"
cache:
    secretName: s3access
```

S3에 접근하기 위한 별도의 access key를 Secret으로  생성합니다.

```bash
kubectl create secret generic s3access \
    --from-literal=accesskey="YourAccessKey" \
    --from-literal=secretkey="YourSecretKey"
```

정상적으로 설정이 되었다면 helm upgrade를 진행합니다.

```bash
helm upgrade gitlab-runner gitlab/gitlab-runner --version 0.48.0 -f values.yaml -n gitlab
```

릴리즈 




## Cache S3 저장 확인

GitLab-Runner에 Cache 동작을 확인하기 위해서는 해당 Runner가 연결된 프로젝트에서 Cache job이 설정된 파이프라인을 동작시켜야 합니다.

동작된 파이프라인의 결과물로 만들어진 Cache는 S3 버킷에서 확인되어야 합니다.


S3 cache가 설정된 Runner가 연결되어있는 project에서 cache job이 있는 파이프라인을 동작시킵니다.

`CICD - Pipeline - Pipeline Run`

job 성공여부 및 실행된 job에서 Cache가 저장되는 로그를 확인합니다.

```bash
Saving cache for successful job
00:03
Creating cache default-protected...
.gradle: found 1319 matching files and directories 
Uploading cache.zip to https://runner-cache.s3.dualstack.ap-northeast-2.amazonaws.com/cache/project....
``` 

정상 동작 확인후 S3에 저장된 Cache 파일을 확인합니다.

```bash
aws s3 ls s3://<버킷 이름>
```
혹은 AWS Console을 통해 S3에 저장된 캐시파일을 확인합니다.
# GitLab-Runner Minio Cache 사용가이드 (Docker)
본 가이드에서는 GitLab-Runner를 사용하는데 필요한 Cache서버 Minio를 내부에 구축하여 사용하는 방법을 가이드합니다.


## Minio 설치 

Minio를 사용하기위해서 Docker 환경을 구축하기위한 별도의 서버가 마련되어야 합니다.
해당 서버는 파이프라인 빌드의 크기와 빈도에 따라 다릅니다.

**서버 정보**
- CPU : 최소 1core 이상
- Memory : 최소 2GB 이상
- Disk : 사용되는 용량에따라 상이합니다. (별도의 /data 저장소 할당)
- OS : CenOS 7
운영체제에 대한 제약은 없으나 본 가이드에서는 SK C&C Runner에서 사용하는 운영체제와 동일하게 생성하는것으로 가정합니다.



**Docker 및 Docker compose설치**

1. 저장소 설정

```bash
 sudo yum install -y yum-utils
 sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
```

2. Docker Engine, Containerd , Docker Compose 설치

```bash
sudo yum install docker-ce docker-ce-cli containerd.io docker-compose-plugin
```

3. 도커 시작 

```bash
sudo systemctl start docker
```


**Minio 설치**

준비된 서버에서 docker-compose를 사용하여 Minio 서버를 구동시킵니다.

```bash
$ cd /data

$ vi docker-compose.yml
----
version: '3'
services:
  minio:
    image: minio/minio:latest
    container_name: minio
    environment:
      MINIO_ROOT_USER: gitlab
      MINIO_ROOT_PASSWORD: qweasd12
    restart: always
    shm_size: '64mb' 
    ports:
      - "9000:9000" #minio 서비스 포트
      - "9001:9001" #minio 콘솔 포트
    volumes:
      - .minio:/root/.minio
      - .export:/export
----
:wq 명령으로 파일 저장

$ docker compose up -d

$ docker ps 
```

Minio가 정상적으로 구동되면 UI접근을 통하여 Bucket 을 생성합니다.

1. 웹 브라우저를 통해 https://<server-ip>:9001 접근합니다
2. Login 정보(docker-compose.yml에 설정된 계정정보 사용)입력 및 로그인
3. `GUI - Bucket - Create Bucket` <br>
버킷 네임(예시: gitlab)을 작성하고 Create Bucket 합니다. 


## GitLab-Runner 설정

GitLab-Runner에 접속하여 Runner의 설정을 담당하는 `config.toml` 파일을 찾아 Cache 관련 설정 사항을 수정합니다.


```bash
find / -name config.toml
vi config.toml

[[runners]]
  name = "skdt-aiip-runner-1"
  url = "https://gitlab.com/"
  token = "Access-Token 정보"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    Type = "s3"
    Path = "cache"
    Shared = false
    [runners.cache.s3]
      AccessKey = "gitlab"
      SecretKey = "qweasd12"
      BucketName = "gitlab"
      Insecure = true
      ServerAddress = "<serverIP>:9000"
```
`:wq` 를 통해 해당 정보를 저장합니다.

GitLab-Runner의 설정 변경 후 별도의 조치를 하지 않아도 자동으로 동기화됩니다.


## Cache Minio 저장 확인

Minio cache가 설정된 Runner가 연결되어있는 project에서 cache job이 있는 파이프라인을 동작시킵니다.

`CICD - Pipeline - Pipeline Run`

job 성공여부 및 실행된 job에서 Cache가 저장되는 로그를 확인합니다.

```bash
Saving cache for successful job
00:03
Creating cache default-protected...
.gradle: found 1319 matching files and directories 
Uploading cache.zip to https://runner-cache.s3.dualstack.ap-northeast-2.amazonaws.com/cache/project....
``` 

정상 동작 확인후 Minio Bucket system에 저장된 Cache 파일을 확인합니다.

# Glossary

> GitLab CI/CD 및 DevOps 관련 Glossary 입니다.

## gitlab-ci.yml

- [variables](https://docs.gitlab.com/ee/ci/yaml/#variables) : job 수준의 변수를 정의
    ```yml
    variables:
        DEPLOY_SITE: "https://example.com/"

    deploy_job:
        stage: deploy
        script:
            - deploy-script --url $DEPLOY_SITE --path "/"

    deploy_review_job:
        stage: deploy
        variables:
            REVIEW_PATH: "/review"
        script:
            - deploy-review-script --url $DEPLOY_SITE --path $REVIEW_PATH
    ```
- [image](https://docs.gitlab.com/ee/ci/yaml/#image) : job에서 실행할 구체적인 Docker image
    ```yml
    default:
        image: ruby:3.0

    rspec:
        script: bundle exec rspec

    rspec 2.7:
        image: registry.example.com/my-group/my-project/ruby:2.7
        script: bundle exec rspec
    ```
- [cache](https://docs.gitlab.com/ee/ci/yaml/#cache) : 현재 실행중인 job과 뒤에 실행될 job 사이에 캐쉬 되어야 할 파일 목록
    - `cache:paths` : 캐쉬 되어야 할 파일 혹은 디렉토리
        ```yml
        rspec:
            script:
                - echo "This job uses a cache."
            cache:
                key: binaries-cache
                paths:
                - binaries/*.apk
                - .config
        ```
- [stages](https://docs.gitlab.com/ee/ci/yaml/#stages) : job의 그룹을 정의
    ```yml
    stages:
      - build
      - test
      - deploy

    job1:
        stage: build
        script:
            - echo "This job compiles code."

    job2:
        stage: test
        script:
            - echo "This job tests the compiled code. It runs when the build stage completes."

    job3:
        script:
            - echo "This job also runs in the test stage".

    job4:
        stage: deploy
        script:
            - echo "This job deploys the code. It runs when the test stage completes."
    ```
- [script](https://docs.gitlab.com/ee/ci/yaml/#script) : 러너에서 수행될 명령어(shell script)
    ```yml
    job1:
        script: "bundle exec rspec"

    job2:
        script:
            - uname -a
            - bundle exec rspec
    ```
    - [before_script](https://docs.gitlab.com/ee/ci/yaml/#before_script) : job 실행 전에 수행되는 명령어
        ```yml
        job:
        before_script:
            - echo "Execute this command before any 'script:' commands."
        script:
            - echo "This command executes after the job's 'before_script' commands."
        ```
    - [after_script](https://docs.gitlab.com/ee/ci/yaml/#before_script) : job 실행 후에 수행되는 명령어
        ```yml
        job:
        script:
            - echo "An example script section."
        after_script:
            - echo "Execute this command after the `script` section completes."
        ```
- [artifacts](https://docs.gitlab.com/ee/ci/yaml/#artifacts) : job이 성공되면 첨부될 파일과 디렉토리 목록
    - `artifacts:path` : 밖으로 바로 링크되지 않는 프로젝트 디렉토리 안의 상대 경로
        ```yml
        artifacts:
        paths:
            - binaries/
            - .config
        ```
- [stage](https://docs.gitlab.com/ee/ci/yaml/#stage) : job이 실행될 stage를 정의
    ```yml
    stages:
    - build
    - test
    - deploy

    job1:
    stage: build
    script:
        - echo "This job compiles code."

    job2:
    stage: test
    script:
        - echo "This job tests the compiled code. It runs when the build stage completes."

    job3:
    script:
        - echo "This job also runs in the test stage".

    job4:
    stage: deploy
    script:
        - echo "This job deploys the code. It runs when the test stage completes."
    ```
- [type](https://docs.gitlab.com/ee/ci/yaml/#globally-defined-types) : `stage`로 deprecated 됨
- [only](https://docs.gitlab.com/ee/ci/yaml/#only--except) : job이 언제 실행될지 정의
    ```yml
    job1:
    script: echo
    only:
        - main
        - /^issue-.*$/
        - merge_requests

    job2:
    script: echo
    except:
        - main
        - /^stable-branch.*$/
        - schedules
    ```
- [services](https://docs.gitlab.com/ee/ci/yaml/#services) : `image`에 연결하여 추가로 사용될 Docker image를 정의
    ```yml
    default:
    image:
        name: ruby:2.6
        entrypoint: ["/bin/bash"]

    services:
        - name: my-postgres:11.7
        alias: db-postgres
        entrypoint: ["/usr/local/bin/db-postgres"]
        command: ["start"]

    before_script:
        - bundle install

    test:
    script:
        - bundle exec rake spec
    ```
- [environment](https://docs.gitlab.com/ee/ci/yaml/#environment) : job에서 배포할 [environment](https://docs.gitlab.com/ee/ci/environments/index.html)를 정의
    ```yml
    deploy to production:
        stage: deploy
        script: git push production HEAD:main
        environment: production
    ```
    - [environment:name](https://docs.gitlab.com/ee/ci/yaml/#environmentname) : environment의 이름을 지정
        ```yml
        deploy to production:
            stage: deploy
            script: git push production HEAD:main
            environment:
                name: production
        ```
    - [environment:url](https://docs.gitlab.com/ee/ci/yaml/#environmenturl) : environment의 url을 지정
        ```yml
        deploy to production:
            stage: deploy
            script: git push production HEAD:main
            environment:
                name: production
                url: https://prod.example.com
        ```

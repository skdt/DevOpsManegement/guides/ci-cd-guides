# CI/CD Guides

* [SonarQube 연동 가이드](01_SonarQube_Spring_Boot_Maven_CI.md) : Java 기반 Spring Boot 애플리케이션의 버그(Bugs), 보안 취약점(Vulnerabilities), 코드 스멜(Code smells) 등을 감지하기 위해 SonarQube와 연동하여 코드의 정적 분석을 수행하는 GitLab CI 파이프라인을 구성하는 방법
* [GitLab CI 기초 가이드](02_GitLab_Basic_CI.md) : GitLab을 이용한 **지속적 통합(Continuous Integration, CI)** 환경 구축하는 방법, CI가 무엇이고, 왜 사용해야 하는지, 그리고 GitLab으로 **CI Pipeline**을 구성하는 방법을 설명
* [GitHub에서 GitLab으로 마이그레이션 가이드](03_Migrating_from_GitHub_to_GitLab.md) : GitHub 리포지토리를 GitLab.com의 프로젝트로 마이그레이션하는 방법
* [GitLab 사용 가이드](04_How_to_Use_GitLab.md) : GitLab의 주요 기능 및 특징을 소개하고 GitLab을 이용하여 프로젝트에서 업무를 수행하는 방법
* [TestProject 연동 가이드](05_Test_Automation_with_TestProject.md) : Java용 TestProject OpenSDK을 사용하여 테스트 코드를 작성하고, TestProject platform(app.testproject.io) 및 TestProject agent와 연동하여 자동화 테스트를 수행하는 GitLab CI 파이프라인을 구성하는 방법
* [JMeter 연동 가이드](06_Performance_Testing_with_JMeter.md) : Apache JMeter를 설치 및 구성하여 GUI에서 Test Plan을 작성한 후, JMeter CLI를 사용하여 Test Plan을 실행하고, HTML Dashboard Report를 생성하는 GitLab CI 파이프라인을 구성하는 방법
* [GitLab Runner 설치 및 등록 가이드](07_Setup_GitLab_Runner.md) : CentOS 7에 Docker Engine과 Docker Compose를 설치한 후, 이를 이용하여 Runner를 설치 및 시작하고 GitLab에 등록하는 방법
* [Bitbucket에서 GitLab으로 마이그레이션 가이드](08_Migrating_from_Bitbucket_to_GitLab.md) : Bitbucket 리포지토리를 GitLab 프로젝트의 리포지토리로 마이그레이션하는 방법
* [Google Kubernetes Engine(GKE) 배포 가이드](09_Deploying_to_GKE.md) : Google Kubernetes Engine(GKE) 클러스터에 배포 환경을 구성하고 GitLab CD 파이프라인으로 GKE 클러스터에 애플리케이션을 배포하는 방법
* [Azure Kubernetes Service(AKS) 배포 가이드](10_Deploying_to_AKS.md) : Azure Kubernetes Service(AKS) 클러스터에 배포 환경을 구성하고 GitLab CD 파이프라인으로 AKS 클러스터에 애플리케이션을 배포하는 방법
